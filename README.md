The programs demonstrate how to implement Greedy Activity Selector, both iteratively and recursively.

1. GreedyActivitySelector.py :
    Program to demonstrate activity selector algorithm iteratively

2. RecursiveActivitySelector.py :
    Program to demonstrate activity selector algorithm recursively
    
3. TestCase.py :
    Program to test each funtions using unittest
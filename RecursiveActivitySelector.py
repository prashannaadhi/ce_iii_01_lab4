#Recursive Activity Selector

A =[]
def RAS(s,f,k,n):
    m = k + 1
    while(m < n and s[m] < f[k] and k >=0):
        m = m + 1
    
    if m < n :
        A.append(m)
        RAS(s,f,m,n)
        return A
    else:
        return 0

#a = RAS([1,3,0,5,3,5,6,8,8,2,12],[4,5,6,7,9,9,10,11,12,14,16],-1,11)
#print(a)